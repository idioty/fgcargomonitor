class FGTownCargos {
	company_id = null;
	town_id = null;
	cargos_delivery = null;
	cargos_pickup = null;
	logLevel = 0;
	
	constructor (a_company_id, a_town_id, a_logLevel) {
		this.company_id = a_company_id;
		this.town_id = a_town_id;
		this.logLevel = a_logLevel;
		this.cargos_delivery = [];
		this.cargos_pickup = [];
	}
}

function FGTownCargos::SaveData() {
	return {
		town_id = this.town_id,
		cargos_delivery = this.cargos_delivery,
		cargos_pickup = this.cargos_pickup,
	}
}

function FGTownCargos::LoadFromData(data) {
	this.town_id = data.town_id;
	this.cargos_delivery = data.cargos_delivery;
	this.cargos_pickup = data.cargos_pickup;
}

function FGTownCargos::UpdateAllMonths(elteltMonths) {
	foreach (cargo_object in this.cargos_delivery) {
		for (local i = cargo_object.months.len(); i <= elteltMonths; i++)
			cargo_object.months.append(0);
	}
	foreach (cargo_object in this.cargos_pickup) {
		for (local i = cargo_object.months.len(); i <= elteltMonths; i++)
			cargo_object.months.append(0);
	}
}

function FGTownCargos::CargoObject(cargo_id, cargos) {
	foreach (cargo_object in cargos) {
		if (cargo_object.cargo_id == cargo_id)
			return cargo_object;
	}
	
	return null;
}

function FGTownCargos::RemoveCargo(cargo_id, cargos) {
	for (local i = 0; i < cargos.len(); i++) {
		if (cargos[i].cargo_id == cargo_id) {
			cargos.remove(i);
			if (this.logLevel >= 3)
				GSLog.Info("[FGTownCargos::RemoveCargo] " + GSCompany.GetName(this.company_id) + " succesfull unsubscrubed from monitoring cargo: " + GSCargo.GetCargoLabel(cargo_id) + " in town: " + GSTown.GetName(this.town_id));
			break;
		}
	}
}
