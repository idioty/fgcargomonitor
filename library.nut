class FGCargoMonitor extends GSLibrary {
	function GetAuthor()      { return "idioty"; }
	function GetName()        { return "FGCargoMonitor"; }
	function GetShortName()   { return "FGCM"; }
	function GetDescription() { return "You can easily subscribe to cargo changes."; }
	function GetAPIVersion()  { return "1.3"; }
	function GetVersion()     { return 5; }
	function GetDate()        { return "2014-01-26"; }
	function GetURL()         { return ""; }
	function CreateInstance() { return "FGCargoMonitor"; }
	function GetCategory()    { return "Util"; }
}

RegisterLibrary(FGCargoMonitor());
