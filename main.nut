
// - # FGCargoMonitor

require("FGMonitoredGoal.nut");
require("FGCompanyCargos.nut");
require("FGTownCargos.nut");
require("FGIndustryCargos.nut");

class FGCargoMonitor {

	// REFRESH_TYPES
	static REFRESH_INVALID = 0;
	static REFRESH_NONE = 1;
	static REFRESH_IMMEDIATELY = 2;
	static REFRESH_EVERY_MONTHS = 3;
	static REFRESH_EVERY_QARTER_YEARS = 4;
	static REFRESH_EVERY_HALF_YEARS = 5;
	static REFRESH_EVERY_YEARS = 6;
	

	// MONITOR_TYPES
	static MONITOR_INVALID = 0;
	static MONITOR_ALL = 1;
	static MONITOR_ALL_DELIVERY = 2;
	static MONITOR_ALL_PICKUP = 3;
	static MONITOR_TOWN_DELIVERY = 4;
	static MONITOR_TOWN_PICKUP = 5;
	static MONITOR_INDUSTRY_DELIVERY = 6;
	static MONITOR_INDUSTRY_PICKUP = 7;


	// LOGLEVEL_TYPES
	static LOGLEVEL_OFF = 0;
	static LOGLEVEL_ERRORS_ONLY = 1;
	static LOGLEVEL_ERRORS_AND_WARNINGS = 2;
	static LOGLEVEL_ALL = 3;


	// LAST_MONTHS_TYPES
	static LAST_MONTH_NONE = -3;
	static LAST_MONTH_ALL = -2;
	static LAST_MONTH_GOAL = -1;
	static LAST_MONTH_CURRENT = 0;
	
	
	monitor_delegate = null; // a delegate az excepted IDENTIFIER volt, azert kellett atirni
	subscribers = null; // ezek iratkoztak fel a frissitesi uzenetekre
	cached_subscribers = null; // ebben fogom tarolni az egy nap alatt erkezo uj feliratkozokat, akiket csak az Update() vegen fogok hozzaadni
	companies = null;
	
	logLevel = 0; // 0: nothing, 1: error only, 2: error and warning, 3: all
	startyear = 0;
	
	log_date_enabled = true; // kell-e date a log kiiratasba

	// nem a havi monitirozast kikapcsolni, hanem a feladat monitorozasan kivul a cegek osszegyujtott cuccait
	// de ez ugye bar csak az adott cargo-t monitorozza, nem az osszeset
	use_only_immediatley_updates = true; // ezzel az ertekkel be lehet kapcsolni a company monitorozasat
	
	constructor (a_delegate, a_logLevel) {
		this.monitor_delegate = a_delegate;
		this.logLevel = a_logLevel;
		this.startyear = GSDate.GetYear(GSDate.GetCurrentDate());
		
		this.subscribers = [];
		this.cached_subscribers = [];
		this.companies = [];
	}
}

// - # Save and Load

function FGCargoMonitor::SaveData() {
	local a_subscribers = [];
	foreach (subscriber in this.subscribers) {
		a_subscribers.append(subscriber.SaveData());
	}
	
	local a_cached_subscribers = [];
	foreach (subscriber in this.cached_subscribers) {
		a_cached_subscribers.append(subscriber.SaveData());
	}
	
	local a_companies = [];
	foreach (company in this.companies) {
		a_companies.append(company.SaveData());
	}
	
	return {
		startyear = this.startyear,
		use_only_immediatley_updates = this.use_only_immediatley_updates,

		subscribers = a_subscribers,
		cached_subscribers = a_cached_subscribers, // ezt is elmentjuk a biztonsag kedveert
		companies = a_companies,
	}
}

function FGCargoMonitor::LoadFromData(loaded_data) {
	this.startyear = loaded_data.startyear;

	// 5-os verziotol eleheto csak
	if (loaded_data.rawin("use_only_immediatley_updates"))
		this.use_only_immediatley_updates = loaded_data.use_only_immediatley_updates;

	// subscriber eseten nem kell a LoadFromData, mert automatikusan ugy hozom letre
	foreach (subscriber_list in loaded_data.subscribers) {
		local subscriber = FGMonitoredGoal(subscriber_list);
		this.subscribers.append(subscriber);
	}
	
	foreach (subscriber_list in loaded_data.cached_subscribers) {
		local subscriber = FGMonitoredGoal(subscriber_list);
		this.cached_subscribers.append(subscriber);
	}
	
	foreach (company_list in loaded_data.companies) {
		local company = FGCompanyCargos(company_list.company_id, this.logLevel);
		company.LoadFromData(company_list);
		this.companies.append(company);
	}
}

// - # Cargo Monitor Setup functions

function FGCargoMonitor::DisableGameDateFromLog() {
	this.log_date_enabled = false;
}

function FGCargoMonitor::SetEnabledCompanyMonitor(enable) {
	if (this.logLevel >= 3 && this.use_only_immediatley_updates != enable) {
		if (!enable)
			GSLog.Info("[FGCargoMonitor::SetEnabledCompanyMonitor] The FGCargoMonitor now use only immediately updates!");
		else
			GSLog.Info("[FGCargoMonitor::SetEnabledCompanyMonitor] The FGCargoMonitor now use all updates!");
	}

	this.use_only_immediatley_updates = !enable;
}

// - # Update

function FGCargoMonitor::Update() {
	// megnezzuk, hogy van-e olyan feliratkozom aki meg nem szerepel a listankban, es akkor hozzaadjuk
	foreach (subscriber in this.subscribers) {
		local van = false;
		foreach (company in this.companies) {
			if (company.company_id == subscriber.company_id)
				van = true;
		}
		
		if (!van) {
			local newCompany = FGCompanyCargos(subscriber.company_id, this.logLevel);
			this.companies.append(newCompany);
		}
	}

	local year = GSDate.GetYear(GSDate.GetCurrentDate());
	local elteltMonths = ((year - this.startyear) * 12) + GSDate.GetMonth(GSDate.GetCurrentDate()) - 1; // azert a -1, hogy 0 legyen a kezdohonap a lista miatt

	// elkuldjuk a feliratkozoknak az elozo havi eredmenyeket
	// azert itt, mert ilyenkor meg nem kerultek bele az uj honapi cuccok
	if (!this.use_only_immediatley_updates && GSDate.GetDayOfMonth(GSDate.GetCurrentDate()) == 1) {
		local currentmonth = GSDate.GetMonth(GSDate.GetCurrentDate());
		currentmonth--; // akkor nem mondjuk a 3 honapban vegen kell frissiteni, hanem a 4 honap elejen, igy akkor lesz jo, ha atlapunk az uj honapba es elso napon nezzuk

		// ha 0-lesz, akkor azt jelenti, hogy az ay elso honap, azaz az eves frissiteseket kell megejteni
		if (currentmonth == 0)
			currentmonth = 12;

		foreach (subscriber in this.subscribers) {
			local send_update = false; // ezzel meggyorsitjuk a folyamatot, ha havi lekerdezes van
			local rm = 0;
			if (subscriber.refresh_interval == FGCargoMonitor.REFRESH_EVERY_MONTHS) {
				send_update = true;
				rm = 1;
			}
			else {
				switch (subscriber.refresh_interval) {
					case FGCargoMonitor.REFRESH_EVERY_QARTER_YEARS:
						rm = 3;
						break;
					case FGCargoMonitor.REFRESH_EVERY_HALF_YEARS:
						rm = 6;
						break;
					case FGCargoMonitor.REFRESH_EVERY_YEARS:
						rm = 12;
						break;
				}
			}

			if (rm == 1 || (rm > 0 && ((currentmonth % rm) == 0))) { // tehat akkor a frissitendo honap utolso napjan vagyunk, kuldhetjuk az update-t
				local transported = subscriber.GetTransported(subscriber.last_months, elteltMonths, true);

				if (transported > 0) {
					this.monitor_delegate.SendCargoUpdate(subscriber.company_id, subscriber.goal_id, transported);
				}

//				GSLog.Info("last month delivery: " + this.AmountOfLastMonthDeliveredCargo(subscriber.company_id, subscriber.cargo_id, 0));
//				GSLog.Info("last month pickup: " + this.AmountOfLastMonthPickedupCargo(subscriber.company_id, subscriber.cargo_id, 0));
			}
		}
	}


	// meg kelzdjuk a frissitest
	foreach (company in this.companies) {
		// elso alkalommal frissitjuk a honapokat, es hozzaadjuk az ujat
		company.UpdateAllMonths(elteltMonths);
		
		// osszegyujtjuk a cargokat, ami all monitorozasuak
		local alredyMonitoredDeliveryTown = [];
		local alredyMonitoredPickupTown = [];
		local alredyMonitoredDeliveryIndustry = [];
		local alredyMonitoredPickupIndustry = [];
		
		// csak ket kerest csinalunk, mert eloszor tudnunk kell az osszes dologra feliratkozokat, mert ezen belul is ertesiteni fogjuk az egyeni feliratkozokat
		foreach (subscriber in this.subscribers) {
			if (company.company_id != subscriber.company_id)
				continue;
			
			// all eseteben eleg csak az osszes varostol lekerdezni
			
			local needMonitoringAllDelivery = false;
			local needMonitoringAllPickup = false;
			
			switch (subscriber.monitor_type) {
				case FGCargoMonitor.MONITOR_ALL:
					needMonitoringAllDelivery = true;
					needMonitoringAllPickup = true;
					break;
				case FGCargoMonitor.MONITOR_ALL_DELIVERY:
					needMonitoringAllDelivery = true;
					break;
				case FGCargoMonitor.MONITOR_ALL_PICKUP:
					needMonitoringAllPickup = true;
					break;
			}
			
			if (needMonitoringAllDelivery) {
				local van = false;
				foreach (cargo_id in alredyMonitoredDeliveryTown) {
					if (cargo_id == subscriber.cargo_id) {
						van = true;
						break;
					}
				}
				
				if (van)
					continue;
				
				// osszes varos osszes delivery-t vesszuk
				local towns = GSTownList();
				foreach (town_id, _ in towns) {
					if (!GSTown.IsValidTown(town_id)) {
						if (this.logLevel >= 2)
							GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::Update] (Delivery All) Invalid town id: " + town_id);
						continue;
					}
					
					local towntransported = GSCargoMonitor.GetTownDeliveryAmount(company.company_id, subscriber.cargo_id, town_id, true);
					if (towntransported > 0) {
						if (!this.use_only_immediatley_updates)
							company.AddDeliveryTownCargo(subscriber.cargo_id, towntransported, elteltMonths, town_id);
						
						// towntransportedet el kell kuldeni
						// elvileg itt azt kuldjuk el, aki konkretan egy varos beerkezesere iratkozott fel
						foreach (asubscriber in this.subscribers) {
							if ((asubscriber.monitor_type == FGCargoMonitor.MONITOR_ALL ||
								asubscriber.monitor_type == FGCargoMonitor.MONITOR_ALL_DELIVERY ||
								(asubscriber.monitor_type == FGCargoMonitor.MONITOR_TOWN_DELIVERY && asubscriber.type_id == town_id)) &&
								asubscriber.company_id == company.company_id &&
								asubscriber.cargo_id == subscriber.cargo_id) {

								if (asubscriber.refresh_interval == FGCargoMonitor.REFRESH_IMMEDIATELY) {
									this.monitor_delegate.SendCargoUpdate(asubscriber.company_id, asubscriber.goal_id, towntransported);
								}

								// a scubscriber-ben is elmentem az osszegyujtott cuccokat, hogy pontosabb legyen a feladat allasanak elkuldese
								asubscriber.AddTransported(towntransported, elteltMonths);
							}
						}
					}
				}

				alredyMonitoredDeliveryTown.append(subscriber.cargo_id);
			}
			
			if (needMonitoringAllPickup) {
				local van = false;
				foreach (cargo_id in alredyMonitoredPickupTown) {
					if (cargo_id == subscriber.cargo_id) {
						van = true;
						break;
					}
				}
				
				if (!van) {
					// osszes varos osszes delivery-t vesszuk
					local industries = GSIndustryList_CargoProducing(subscriber.cargo_id);
					local towns = GSTownList();
					local appendToIndustry = false;
					foreach (town_id, _ in towns) {
						if (!GSTown.IsValidTown(town_id)) {
							if (this.logLevel >= 2)
								GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::Update] (Pickup All) Invalid town id: " + town_id);
							continue;
						}
						
					
						local towntransported = GSCargoMonitor.GetTownPickupAmount(subscriber.company_id, subscriber.cargo_id, town_id, true);

						{
							// TODO: ezt a reszt kell kivenni, ha a nightlyban javitjak a hibat,
							// pontosabban le kell kerdezni a verzioszamot, ahonnan mar jo, es ha a feletti,
							// akkor nem kell ezt a reszt futtatni, tehat a regi verzio tamogatas miatt is kell

							// tehat valami olyasmi a baj, hogy a town_delivery-ben benne van az osszes industry_delivery is
							// de a town pickupban nincsenek benne az industry_pickupok
							// ezert kerdezem le itt az osszes industry_pickupot

							// ebben az esetben az industy-t is ellenorizni kell
							van = false;
							foreach (cargo_id in alredyMonitoredPickupIndustry) {
								if (cargo_id == subscriber.cargo_id) {
									van = true;
									break;
								}
							}

							if (!van) {
								foreach (industry_id, _ in industries) {
									if (!GSIndustry.IsValidIndustry(industry_id)) {
										if (this.logLevel >= 2)
											GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::Update] (Pickup All Industry) Invalid industry id: " + industry_id);
										continue;
									}

									// ha nem egyezik a varos azzal, amit le akarunk kerdezni, akkor nem erdekel minket
									local ind_tile = GSIndustry.GetLocation(industry_id);
									local a_town_id = GSTile.GetTownAuthority(ind_tile);
									if (!GSTown.IsValidTown(a_town_id))
										a_town_id = GSTile.GetClosestTown(ind_tile);


									if (a_town_id != town_id)
										continue;



									local industrytransported = GSCargoMonitor.GetIndustryPickupAmount(subscriber.company_id, subscriber.cargo_id, industry_id, true);
									if (industrytransported > 0) {
										if (!this.use_only_immediatley_updates)
											company.AddPickupIndustryCargo(subscriber.cargo_id, industrytransported, elteltMonths, industry_id);

										towntransported += industrytransported;

										// towntransportedet el kell kuldeni
										// elvileg itt azt kuldjuk el, aki konkretan egy varos beerkezesere iratkozott fel
										foreach (asubscriber in this.subscribers) {
											if (asubscriber.monitor_type == FGCargoMonitor.MONITOR_INDUSTRY_PICKUP &&
												asubscriber.type_id == industry_id &&
												asubscriber.company_id == company.company_id &&
												asubscriber.cargo_id == subscriber.cargo_id) {

												if (asubscriber.refresh_interval == FGCargoMonitor.REFRESH_IMMEDIATELY) {
													this.monitor_delegate.SendCargoUpdate(asubscriber.company_id, asubscriber.goal_id, industrytransported);
												}

												// a scubscriber-ben is elmentem az osszegyujtott cuccokat, hogy pontosabb legyen a feladat allasanak elkuldese
												asubscriber.AddTransported(industrytransported, elteltMonths);
											}
										}
									}

									appendToIndustry = true;
								}
							}
						}
						
						if (towntransported > 0) {
							if (!this.use_only_immediatley_updates)
								company.AddPickupTownCargo(subscriber.cargo_id, towntransported, elteltMonths, town_id);
							
							// towntransportedet el kell kuldeni
							// elvileg itt azt kuldjuk el, aki konkretan egy varos beerkezesere iratkozott fel
							foreach (asubscriber in this.subscribers) {
								if ((asubscriber.monitor_type == FGCargoMonitor.MONITOR_ALL ||
									asubscriber.monitor_type == FGCargoMonitor.MONITOR_ALL_PICKUP ||
									(asubscriber.monitor_type == FGCargoMonitor.MONITOR_TOWN_PICKUP && asubscriber.type_id == town_id)) &&
									asubscriber.company_id == company.company_id &&
									asubscriber.cargo_id == subscriber.cargo_id) {

									if (asubscriber.refresh_interval == FGCargoMonitor.REFRESH_IMMEDIATELY) {
										this.monitor_delegate.SendCargoUpdate(asubscriber.company_id, asubscriber.goal_id, towntransported);
									}

									// a scubscriber-ben is elmentem az osszegyujtott cuccokat, hogy pontosabb legyen a feladat allasanak elkuldese
									asubscriber.AddTransported(towntransported, elteltMonths);
								}
							}
						}
					}

					alredyMonitoredPickupTown.append(subscriber.cargo_id);

					if (appendToIndustry)
						alredyMonitoredPickupIndustry.append(subscriber.cargo_id);
				}
			}
		}

		// itt a masik kereses, itt mar csak az egyeni feliratkozok az erdekesek
		// azert csinalom meg ketkorosre, mert a town_pickupnal kell tudnunk az industry_pickupot is,
		// igy elso korben nem ellenorizzuk az inudstry_pickupot
		local kor = 1;
		while (kor <= 2) {
			kor++;
			foreach (subscriber in this.subscribers) {
				if (company.company_id != subscriber.company_id)
					continue;
				local alredyMonitored = null;

				switch (subscriber.monitor_type) {
					case FGCargoMonitor.MONITOR_TOWN_DELIVERY:
						if (kor == 2) // elso korben frissitjuk az ilyesmit csak
							alredyMonitored = alredyMonitoredDeliveryTown;
						break;
					case FGCargoMonitor.MONITOR_TOWN_PICKUP:
						if (kor == 2) // elso korben frissitjuk az ilyesmit csak
							alredyMonitored = alredyMonitoredPickupTown;
						break;
					case FGCargoMonitor.MONITOR_INDUSTRY_DELIVERY:
						if (kor == 2) // elso korben frissitjuk az ilyesmit csak
							alredyMonitored = alredyMonitoredDeliveryIndustry;
						break;
					case FGCargoMonitor.MONITOR_INDUSTRY_PICKUP:
						if (kor > 2) // elso korben nem szabad ezt megnezni csak a masodik korben, a kor++ miatt kell itt nagyobb 2...
							alredyMonitored = alredyMonitoredPickupIndustry;
						break;
				}

				if (alredyMonitored == null)
					continue;

				// megkeressuk, hogy nincs-e mar a monitorozott dolgok kozott
				local van = false;
				foreach (cargo_id in alredyMonitored) {
					if (cargo_id == subscriber.cargo_id) {
						van = true;
						break;
					}
				}
				
				if (van) // ha van, akkor mar elvileg kapott mindenki ertesitest
					continue;

				local hibavan = false;
				switch (subscriber.monitor_type) {
					case FGCargoMonitor.MONITOR_TOWN_DELIVERY:
					case FGCargoMonitor.MONITOR_TOWN_PICKUP:
						if (!GSTown.IsValidTown(subscriber.type_id)) {
							if (this.logLevel >= 2)
								GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::Update] Invalid town_id: " + subscriber.type_id);
							hibavan = true;
						}
						break;
					case FGCargoMonitor.MONITOR_INDUSTRY_DELIVERY:
					case FGCargoMonitor.MONITOR_INDUSTRY_PICKUP:
						if (!GSIndustry.IsValidIndustry(subscriber.type_id)) {
							if (this.logLevel >= 2)
								GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::Update] Invalid industry_id: " + subscriber.type_id);
							hibavan = true;
						}
						break;
				}
				
				if (hibavan)
					continue;

				// muszaj kulon, mert tudnunk kell, hogy valos-e az id
				local transported = 0;
				switch (subscriber.monitor_type) {
					case FGCargoMonitor.MONITOR_TOWN_DELIVERY:
						transported = GSCargoMonitor.GetTownDeliveryAmount(company.company_id, subscriber.cargo_id, subscriber.type_id, true);
						break;
					case FGCargoMonitor.MONITOR_TOWN_PICKUP:
						transported = GSCargoMonitor.GetTownPickupAmount(company.company_id, subscriber.cargo_id, subscriber.type_id, true);
						// TODO: ezt is ki kell venni, ha megoldjak a townpickupot industry nelkul (mint fentebb)
						// ebben az esetben az osszes varoshoz tartozo gyarat le kell ellenorizni, hogy nem szallitottak oda cuccot
						{
							local van = false;
							foreach (cargo_id in alredyMonitoredPickupIndustry) {
								if (cargo_id == subscriber.cargo_id) {
									van = true;
									break;
								}
							}

							if (!van) {
								// osszes varos osszes delivery-t vesszuk
								local industries = GSIndustryList_CargoProducing(subscriber.cargo_id);
								foreach (industry_id, _ in industries) {
									if (!GSIndustry.IsValidIndustry(industry_id)) {
										if (this.logLevel >= 2)
											GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::Update] (Pickup All Industry) Invalid industry id: " + industry_id);
										continue;
									}

									// ha nem egyezik a varos azzal, amit le akarunk kerdezni, akkor nem erdekel minket
									local ind_tile = GSIndustry.GetLocation(industry_id);
									local a_town_id = GSTile.GetTownAuthority(ind_tile);
									if (!GSTown.IsValidTown(a_town_id))
										a_town_id = GSTile.GetClosestTown(ind_tile);

									if (a_town_id != subscriber.type_id)
										continue;

									local industrytransported = GSCargoMonitor.GetIndustryPickupAmount(subscriber.company_id, subscriber.cargo_id, industry_id, true);
									if (industrytransported > 0) {
										if (!this.use_only_immediatley_updates)
											company.AddPickupIndustryCargo(subscriber.cargo_id, industrytransported, elteltMonths, industry_id);

										transported += industrytransported;
										
										// towntransportedet el kell kuldeni
										// elvileg itt azt kuldjuk el, aki konkretan egy varos beerkezesere iratkozott fel
										foreach (asubscriber in this.subscribers) {
											// csak az industry pickupot hagyom benne, mert duplan kuldene el a tobbinek, ha mast is benne hagynek....
											// bar az is igaz, hogy a monitor_all tipusok mar ezt a cargo-t elvittek volna innen... de biztos ami biztos

											if (asubscriber.monitor_type == FGCargoMonitor.MONITOR_INDUSTRY_PICKUP &&
												asubscriber.type_id == industry_id &&
												asubscriber.company_id == company.company_id &&
												asubscriber.cargo_id == subscriber.cargo_id) {

												if (asubscriber.refresh_interval == FGCargoMonitor.REFRESH_IMMEDIATELY) {
													this.monitor_delegate.SendCargoUpdate(asubscriber.company_id, asubscriber.goal_id, industrytransported);
												}

												// a scubscriber-ben is elmentem az osszegyujtott cuccokat, hogy pontosabb legyen a feladat allasanak elkuldese
												asubscriber.AddTransported(industrytransported, elteltMonths);
											}
										}
									}
									
									alredyMonitoredPickupIndustry.append(subscriber.cargo_id);
								}
							}
						}
						
						break;
					case FGCargoMonitor.MONITOR_INDUSTRY_DELIVERY:
						transported = GSCargoMonitor.GetIndustryDeliveryAmount(company.company_id, subscriber.cargo_id, subscriber.type_id, true);
						break;
					case FGCargoMonitor.MONITOR_INDUSTRY_PICKUP:
						transported = GSCargoMonitor.GetIndustryPickupAmount(company.company_id, subscriber.cargo_id, subscriber.type_id, true);
						break;
				}

				if (transported > 0) {
					// termeszetesen a ceg altal elszallitott cuccokhoz is bekerul
					if (!this.use_only_immediatley_updates) {
						switch (subscriber.monitor_type) {
							case FGCargoMonitor.MONITOR_TOWN_DELIVERY:
								company.AddDeliveryTownCargo(subscriber.cargo_id, transported, elteltMonths, subscriber.type_id);
								break;
							case FGCargoMonitor.MONITOR_TOWN_PICKUP:
								company.AddPickupTownCargo(subscriber.cargo_id, transported, elteltMonths, subscriber.type_id);
								break;
							case FGCargoMonitor.MONITOR_INDUSTRY_DELIVERY:
								company.AddDeliveryIndustryCargo(subscriber.cargo_id, transported, elteltMonths, subscriber.type_id);
								break;
							case FGCargoMonitor.MONITOR_INDUSTRY_PICKUP:
								company.AddPickupIndustryCargo(subscriber.cargo_id, transported, elteltMonths, subscriber.type_id);
								break;
						}
					}
					
					// csak azokat frissitjuk, aki nem all-ra iratkozott fel, hanem csak kifejezetten town delivery-re
					foreach (asubscriber in this.subscribers) {
						if (asubscriber.company_id == subscriber.company_id &&
							asubscriber.cargo_id == subscriber.cargo_id &&
							asubscriber.monitor_type == subscriber.monitor_type) {

							if (asubscriber.refresh_interval == FGCargoMonitor.REFRESH_IMMEDIATELY) {
								this.monitor_delegate.SendCargoUpdate(asubscriber.company_id, asubscriber.goal_id, transported);
							}

							// a scubscriber-ben is elmentem az osszegyujtott cuccokat, hogy pontosabb legyen a feladat allasanak elkuldese
							asubscriber.AddTransported(transported, elteltMonths);
						}
					}
				}
				
				alredyMonitored.append(subscriber.cargo_id);
			}
		}
	}
	
	// feliratkoztatjuk a cached-ben levo varakozokat
	if (cached_subscribers.len() > 0) {
		foreach (subscriber in this.cached_subscribers) {
			this.subscribers.append(subscriber);
		}
		
		cached_subscribers.clear();
	}
}

// - # Subscribe and unsbscribe functions

function FGCargoMonitor::IsSubscribedToMonitor(company_id, goal_id) {
	if (company_id == null || GSCompany.ResolveCompanyID(company_id) == GSCompany.COMPANY_INVALID) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::IsSubscribedToMonitor] Can not use invalid company_id");
		return false;
	}

	if (goal_id == null) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::IsSubscribedToMonitor] Can not suse invalid goal_id (null)");
		return false;
	}

	foreach (subscriber in this.subscribers) {
		if (subscriber.company_id == company_id && subscriber.goal_id == goal_id)
			return true;
	}

	return false;
}

function FGCargoMonitor::SubscribeToMonitor(company_id, cargo_id, goal_id, refresh_interval, monitor_type, type_id, last_months) {
	if (company_id == null || GSCompany.ResolveCompanyID(company_id) == GSCompany.COMPANY_INVALID) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::SubscribeToMonitor] Can not subscribe, because invalid company_id");
		return false;
	}
	
	if (cargo_id == null || !GSCargo.IsValidCargo(cargo_id)) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::SubscribeToMonitor] Can not subscribe, because invalid cargo_id");
		return false;
	}
	
	if (goal_id == null) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::SubscribeToMonitor] Can not subscribe to cargo: " + GSCargo.GetCargoLabel(cargo_id) + ", because invalid goal_id");
		return false;
	}
	
	if (!FGCargoMonitor.IsValidRefreshInterval(refresh_interval)) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::SubscribeToMonitor] Can not subscribe to cargo: " + GSCargo.GetCargoLabel(cargo_id) + ", because invalid refresh_interval");
		return false;
	}
	
	if ((monitor_type == FGCargoMonitor.MONITOR_TOWN_DELIVERY || monitor_type == FGCargoMonitor.MONITOR_TOWN_PICKUP) && type_id != null && !GSTown.IsValidTown(type_id)) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::SubscribeToMonitor] Can not subscribe to cargo: " + GSCargo.GetCargoLabel(cargo_id) + ", because invalid town_id (type_id)");
		return false;
	}
	
	if ((monitor_type == FGCargoMonitor.MONITOR_INDUSTRY_DELIVERY || monitor_type == FGCargoMonitor.MONITOR_INDUSTRY_PICKUP) && type_id != null && !GSIndustry.IsValidIndustry(type_id)) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::SubscribeToMonitor] Can not subscribe to cargo: " + GSCargo.GetCargoLabel(cargo_id) + ", because invalid industry_id (type_id)");
		return false;
	}

	// itt azert lekerdezzuk, hogy hozza van-e mar adva ezzel a company_id es goal_id-vel egy feliratkozas
	if (this.IsSubscribedToMonitor(company_id, goal_id)) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::SubscribeToMonitor] Can not subscribe to cargo: " + GSCargo.GetCargoLabel(cargo_id) + ", because alredy subscribed company with this goal_id: " + goal_id + ")");
		return false;
	}
	
	// azert nem csinalok itt mar rogton egy feliratkozast, mert ugyis naponta frissitem, es akkor megtortenik.
	// ezaltal az van, hogy ha egyvalakinek van csak abbol az adott cargo-bol feladata, elvegzi, torlodik a monitorozas, majd megint megkapja az az adatot, akkor legalabb torlodott az elozo eredmenye.
	
	local data = {
		company_id = company_id,
		cargo_id = cargo_id,
		goal_id = goal_id,
		refresh_interval = refresh_interval,
		monitor_type = monitor_type,
		type_id = type_id,
		months = [],
		last_months = last_months,
	}
	
	local monitored_goal = FGMonitoredGoal(data);
	this.cached_subscribers.append(monitored_goal);

	if (this.logLevel >= 3) {
		GSLog.Info(this.GetLogGameDateString() + "[FGCargoMonitor::SubscribeToMonitor] " + GSCompany.GetName(company_id) + " succesfull subscribed to goal_id: " + goal_id + "!");
		GSLog.Info(".     description: " + monitored_goal.Description() + ".");
	}
	
	return true;
}

function FGCargoMonitor::UnSubscribeFromMonitor(company_id, goal_id) {
	if (company_id == null) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::UnSubscribeFromMonitor] Can not unsubscribe, because invalid company_id");
		return false;
	}
	
	local compname = GSCompany.GetName(company_id);
	if (compname == null || compname == "")
		compname = "(Bankrupted or Merged Company)";
	
	if (this.logLevel >= 3)
		GSLog.Info(this.GetLogGameDateString() + "[FGCargoMonitor::UnSubscribeFromMonitor] " + compname + " begin unsubscribe from CargoMonitor, goal_id: " + goal_id);
	
	
	local subscriber = null;
	for (local i = 0; i < this.subscribers.len(); i++) {
		local asubscriber = this.subscribers[i];
		if (asubscriber.company_id == company_id && asubscriber.goal_id == goal_id) {
			subscriber = asubscriber;
			this.subscribers.remove(i);
			if (this.logLevel >= 3) {
				GSLog.Info(this.GetLogGameDateString() + "[FGCargoMonitor::UnSubscribeFromMonitor] " + compname + " succesfull removed from subscribers. Monitored cargo: " + GSCargo.GetCargoLabel(asubscriber.cargo_id));
				GSLog.Info("......description of subscription: " + asubscriber.Description() + ".");
			}
			break;
		}
	}
	
	if (subscriber == null) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::UnSubscribeFromMonitor] " + compname + " not have monitored goal_id: " + goal_id);
		return false;
	}
	
	local company = this.CompanyFromID(company_id);
	if (company == null) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::UnSubscribeFromMonitor] " + compname + " not have valid tracking object");
		return false;
	}
	
	return company.Remove(subscriber, this.subscribers);
}

function FGCargoMonitor::UnSubscribeCompanyFromMonitor(company_id) {
	if (company_id == null) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::UnSubscribeCompanyFromMonitor] Can not unsubscribe, because invalid company_id");
		return false;
	}

	local mindeneltavolitva = true;
	
	for (local i = 0; i < this.companies.len(); i++) {
		if (this.companies[i].company_id == company_id) {
			local company = this.companies[i];

			for (local j = 0; j < this.subscribers.len(); j++) { // igy nem lesz gond az eltavolitas miatt...
				local subscriber = this.subscribers[j];
				if (company.company_id == subscriber.company_id) {
					if (!this.UnSubscribeFromMonitor(company.company_id, subscriber.goal_id))
						mindeneltavolitva = false;
					// az unsubscribernel eltavolitom, ezert nem kell itt.
					j--;
				}
			}

			this.companies.remove(i);
			break;
		}
	}

	return mindeneltavolitva;
}

function FGCargoMonitor::UnSubscribeAllFromMonitor() {
	// azert nem hasznaljuk a FGCargoMonitor::UnSubscribeCompanyFromMonitor funkciot, mert meg egyszer tok feleslegesen lefuttatna az osszes companies-t


	local mindeneltavolitva = true;

	foreach (subscriber in this.subscribers) {
		if (!this.UnSubscribeFromMonitor(subscriber.company_id, subscriber.goal_id))
			mindeneltavolitva = false;
	}
	
	this.companies.clear();
	this.subscribers.clear();
	this.cached_subscribers.clear();
	GSCargoMonitor.StopAllMonitoring();

	return mindeneltavolitva
}

// - # Company Monitor Questions

function FGCargoMonitor::AmountOfLastMonthDeliveredCargo(company_id, cargo_id, last_months) {
	if (this.use_only_immediatley_updates) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::AmountOfLastMonthDeliveredCargo] Can not return valid value, because 'SetEnabledCompanyMonitor' is off!");

		return 0;
	}

	if (company_id == null) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::AmountOfLastMonthDeliveredCargo] Can not ask, because invalid company_id");
		return 0;
	}
	
	if (cargo_id == null) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::AmountOfLastMonthDeliveredCargo] Can not ask, because invalid cargo_id");
		return 0;
	}
	
	if (last_months <= 0)
		return this.AmountOfAllDeliveredCargo(company_id, cargo_id);

	foreach (company in this.companies) {
		if (company.company_id == company_id) {
			local transported = 0;

			local delivery_cargo_object = company.CargoObject(cargo_id, company.cargos_town_delivery);
			if (delivery_cargo_object != null) {

				if (last_months == 1)
					return delivery_cargo_object.months[delivery_cargo_object.months.len() - 1];

				local from = delivery_cargo_object.months.len() - last_months;
				if (from < 0)
					from = 0;
				
				for (local i = from; i < delivery_cargo_object.months.len(); i++)
					transported += delivery_cargo_object.months[i];
				
			}
			
			return transported;
			break;
		}
	}
	
	return 0;
}

function FGCargoMonitor::AmountOfAllDeliveredCargo(company_id, cargo_id) {
	if (this.use_only_immediatley_updates) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::AmountOfAllDeliveredCargo] Can not return valid value, because 'SetEnabledCompanyMonitor' is off!");

		return 0;
	}

	if (company_id == null) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::AmountOfAllDeliveredCargo] Can not ask, because invalid company_id");
		return 0;
	}
	
	if (cargo_id == null) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::AmountOfAllDeliveredCargo] Can not ask, because invalid cargo_id");
		return 0;
	}
	
	foreach (company in this.companies) {
		if (company.company_id == company_id) {
			local transported = 0;
			
			local delivery_cargo_object = company.CargoObject(cargo_id, company.cargos_town_delivery);
			if (delivery_cargo_object != null) {
				for (local i = 0; i < delivery_cargo_object.months.len(); i++)
					transported += delivery_cargo_object.months[i];
			}
			
			return transported;
			
			break;
		}
	}
	
	return 0;
}

function FGCargoMonitor::AmountOfLastMonthPickedupCargo(company_id, cargo_id, last_months) {
	if (this.use_only_immediatley_updates) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::AmountOfLastMonthPickedupCargo] Can not return valid value, because 'SetEnabledCompanyMonitor' is off!");

		return 0;
	}

	if (company_id == null) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::AmountOfLastMonthPickedupCargo] Can not ask, because invalid company_id");
		return 0;
	}
	
	if (cargo_id == null) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::AmountOfLastMonthPickedupCargo] Can not ask, because invalid cargo_id");
		return 0;
	}
	
	if (last_months <= 0)
		return this.AmountOfAllPickedupCargo(company_id, cargo_id);

	foreach (company in this.companies) {
		if (company.company_id == company_id) {
			local transported = 0;
			
			local pickup_cargo_object = company.CargoObject(cargo_id, company.cargos_town_pickup);
			if (pickup_cargo_object != null) {

				if (last_months == 1)
					return pickup_cargo_object.months[pickup_cargo_object.months.len() - 1];
				
				local from = pickup_cargo_object.months.len() - last_months;
				if (from < 0)
					from = 0;
				
				for (local i = from; i < pickup_cargo_object.months.len(); i++)
					transported += pickup_cargo_object.months[i];
				
			}
			
			return transported;
			break;
		}
	}
	
	return 0;
}

function FGCargoMonitor::AmountOfAllPickedupCargo(company_id, cargo_id) {
	if (this.use_only_immediatley_updates) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::AmountOfAllPickedupCargo] Can not return valid value, because 'SetEnabledCompanyMonitor' is off!");

		return 0;
	}

	if (company_id == null) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::AmountOfAllPickedupCargo] Can not ask, because invalid company_id");
		return 0;
	}
	
	if (cargo_id == null) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::AmountOfAllPickedupCargo] Can not ask, because invalid cargo_id");
		return 0;
	}
	
	foreach (company in this.companies) {
		if (company.company_id == company_id) {
			local transported = 0;
			
			local pickup_cargo_object = company.CargoObject(cargo_id, company.cargos_town_pickup);
			if (pickup_cargo_object != null) {
				for (local i = 0; i < pickup_cargo_object.months.len(); i++)
					transported += pickup_cargo_object.months[i];
			}
			
			return transported;
			
			break;
		}
	}
	
	return 0;
}

// - # Goal Monitor Questions

function FGCargoMonitor::AmountOfGoal(company_id, goal_id) {
	if (company_id == null) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::AmountOfLastMonthGoal] Can not ask, because invalid company_id");
		return 0;
	}

	if (goal_id == null) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::AmountOfLastMonthGoal] Can not ask, because invalid goal_id");
		return 0;
	}


	local year = GSDate.GetYear(GSDate.GetCurrentDate());
	local elteltMonths = ((year - this.startyear) * 12) + GSDate.GetMonth(GSDate.GetCurrentDate()) - 1; // azert a -1, hogy 0 legyen a kezdohonap a lista miatt

	foreach (subscriber in this.subscribers) {
		if (subscriber.company_id == company_id && subscriber.goal_id == goal_id) {
			return subscriber.GetTransported(subscriber.last_months, elteltMonths, false);
			break;
		}
	}

	return 0;
}

function FGCargoMonitor::AmountOfLastMonthGoal(company_id, goal_id, last_months) {
	if (company_id == null) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::AmountOfLastMonthGoal] Can not ask, because invalid company_id");
		return 0;
	}
	
	if (goal_id == null) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::AmountOfLastMonthGoal] Can not ask, because invalid goal_id");
		return 0;
	}


	local year = GSDate.GetYear(GSDate.GetCurrentDate());
	local elteltMonths = ((year - this.startyear) * 12) + GSDate.GetMonth(GSDate.GetCurrentDate()) - 1; // azert a -1, hogy 0 legyen a kezdohonap a lista miatt

	foreach (subscriber in this.subscribers) {
		if (subscriber.company_id == company_id && subscriber.goal_id == goal_id) {
			if (last_months < -1)
				last_months = -1;
			return subscriber.GetTransported(last_months, elteltMonths, false);
			break;
		}
	}

	return 0;
}

function FGCargoMonitor::AmountOfAllGoal(company_id, goal_id) {
	if (company_id == null) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::AmountOfAllGoal] Can not ask, because invalid company_id");
		return 0;
	}
	
	if (goal_id == null) {
		if (this.logLevel >= 2)
			GSLog.Warning(this.GetLogGameDateString() + "[FGCargoMonitor::AmountOfAllGoal] Can not ask, because invalid goal_id");
		return 0;
	}

	local year = GSDate.GetYear(GSDate.GetCurrentDate());
	local elteltMonths = ((year - this.startyear) * 12) + GSDate.GetMonth(GSDate.GetCurrentDate()) - 1; // azert a -1, hogy 0 legyen a kezdohonap a lista miatt

	foreach (subscriber in this.subscribers) {
		if (subscriber.company_id == company_id && subscriber.goal_id == goal_id) {
			return subscriber.GetTransported(FGCargoMonitor.LAST_MONTH_ALL, elteltMonths, false);
			break;
		}
	}

	return 0;
}

// - # Other functions

function FGCargoMonitor::IsValidRefreshInterval(refresh_interval) {
	if (refresh_interval == null)
		return false;

	switch (refresh_interval) {
		case FGCargoMonitor.REFRESH_NONE:
		case FGCargoMonitor.REFRESH_IMMEDIATELY:
		case FGCargoMonitor.REFRESH_EVERY_MONTHS:
		case FGCargoMonitor.REFRESH_EVERY_QARTER_YEARS:
		case FGCargoMonitor.REFRESH_EVERY_HALF_YEARS:
		case FGCargoMonitor.REFRESH_EVERY_YEARS:
			return true;
	}

	return false;
}

//
// - # Private functions
//


function FGCargoMonitor::GetLogGameDateString() {
	if (!this.log_date_enabled)
		return "";

	local date = GSDate.GetCurrentDate();
	local month = GSDate.GetMonth(date);
	local day = GSDate.GetDayOfMonth(date);

	return "[" + GSDate.GetYear(date) + ". " + (month < 10 ? ("0" + month) : month) + ". " + (day < 10 ? ("0" + day) : day) + ".]";
}

function FGCargoMonitor::CompanyFromID(company_id) {
	foreach (company in this.companies) {
		if (company.company_id == company_id)
			return company;
	}
	
	return null;
}

function FGCargoMonitor::MonitorTypeName(monitor_type) {
	switch (monitor_type) {
		case FGCargoMonitor.MONITOR_INVALID:
			return "MONITOR_INVALID";
			break;
		case FGCargoMonitor.MONITOR_ALL:
			return "MONITOR_ALL";
			break;
		case FGCargoMonitor.MONITOR_ALL_DELIVERY:
			return "MONITOR_ALL_DELIVERY";
			break;
		case FGCargoMonitor.MONITOR_ALL_PICKUP:
			return "MONITOR_ALL_PICKUP";
			break;
		case FGCargoMonitor.MONITOR_TOWN_DELIVERY:
			return "MONITOR_TOWN_DELIVERY";
			break;
		case FGCargoMonitor.MONITOR_TOWN_PICKUP:
			return "MONITOR_TOWN_PICKUP";
			break;
		case FGCargoMonitor.MONITOR_INDUSTRY_DELIVERY:
			return "MONITOR_INDUSTRY_DELIVERY";
			break;
		case FGCargoMonitor.MONITOR_INDUSTRY_PICKUP:
			return "MONITOR_INDUSTRY_PICKUP";
			break;
	}

	return "MONITOR_UNKOWN";
}

function FGCargoMonitor::RefreshTypeName(refresh_type) {
	switch (refresh_type) {
		case FGCargoMonitor.REFRESH_INVALID:
			return "REFRESH_INVALID";
			break;
		case FGCargoMonitor.REFRESH_IMMEDIATELY:
			return "REFRESH_IMMEDIATELY";
			break;
		case FGCargoMonitor.REFRESH_EVERY_MONTHS:
			return "REFRESH_EVERY_MONTHS";
			break;
		case FGCargoMonitor.REFRESH_EVERY_QARTER_YEARS:
			return "REFRESH_EVERY_QARTER_YEARS";
			break;
		case FGCargoMonitor.REFRESH_EVERY_HALF_YEARS:
			return "REFRESH_EVERY_HALF_YEARS";
			break;
		case FGCargoMonitor.REFRESH_EVERY_YEARS:
			return "REFRESH_EVERY_YEARS";
			break;
	}

	return "REFRESH_UNKOWN";
}
