class FGCompanyCargos {
	company_id = null;
	logLevel = 0; // kikapcsolva


//	cargos_delivery
//	cargos_pickup

	cargos_town_delivery = null;
	cargos_town_pickup = null;
	cargos_industry_delivery = null;
	cargos_industry_pickup = null;
	
	town_delivery = null;
	town_pickup = null;
	
	industry_delivery = null;
	industry_pickup = null;
	
	constructor (a_company_id, aLogLevel) {
		this.company_id = a_company_id;
		this.logLevel = aLogLevel;
		
		this.cargos_town_delivery = [];
		this.cargos_town_pickup = [];
		this.cargos_industry_delivery = [];
		this.cargos_industry_pickup = [];

		this.town_delivery = [];
		this.town_pickup = [];
		this.industry_delivery = [];
		this.industry_pickup = [];
	}
}

function FGCompanyCargos::SaveData() {
	local a_town_delivery = [];
	local a_town_pickup = [];
	local a_industry_delivery = [];
	local a_industry_pickup = [];
	
	foreach (town_object in this.town_delivery) {
		a_town_delivery.append(town_object.SaveData());
	}
	
	foreach (town_object in this.town_pickup) {
		a_town_pickup.append(town_object.SaveData());
	}
	
	foreach (industry_object in this.industry_delivery) {
		a_industry_delivery.append(industry_object.SaveData());
	}
	
	foreach (industry_object in this.industry_pickup) {
		a_industry_pickup.append(industry_object.SaveData());
	}
	
	return {
		company_id = this.company_id,
		
		cargos_town_delivery = this.cargos_town_delivery,
		cargos_town_pickup = this.cargos_town_pickup,

		cargos_industry_delivery = this.cargos_industry_delivery,
		cargos_industry_pickup = this.cargos_industry_pickup,

		town_delivery = a_town_delivery,
		town_pickup = a_town_pickup,
		
		industry_delivery = a_industry_delivery,
		industry_pickup = a_industry_pickup,
	}
}

function FGCompanyCargos::LoadFromData(data) {
	this.company_id = data.company_id;
	
	this.cargos_town_delivery = data.cargos_town_delivery;
	this.cargos_town_pickup = data.cargos_town_pickup;
	this.cargos_industry_delivery = data.cargos_industry_delivery;
	this.cargos_industry_pickup = data.cargos_industry_pickup;

	foreach (town_object_list in data.town_delivery) {
		local town_object = FGTownCargos(this.company_id, town_object_list.town_id, this.logLevel);
		town_object.LoadFromData(town_object_list);
		this.town_delivery.append(town_object);
	}
	
	foreach (town_object_list in data.town_pickup) {
		local town_object = FGTownCargos(this.company_id, town_object_list.town_id, this.logLevel);
		town_object.LoadFromData(town_object_list);
		this.town_pickup.append(town_object);
	}
	
	foreach (industry_object_list in data.industry_delivery) {
		local industry_object = FGIndustryCargos(this.company_id, industry_object_list.industry_id, this.logLevel);
		industry_object.LoadFromData(industry_object_list);
		this.industry_delivery.append(industry_object);
	}
	
	foreach (industry_object_list in data.industry_pickup) {
		local industry_object = FGIndustryCargos(this.company_id, industry_object_list.industry_id, this.logLevel);
		industry_object.LoadFromData(industry_object_list);
		this.industry_pickup.append(industry_object);
	}
}

function FGCompanyCargos::UpdateAllMonths(elteltMonths) {
	foreach (cargo_object in this.cargos_town_delivery) {
		for (local i = cargo_object.months.len(); i <= elteltMonths; i++)
			cargo_object.months.append(0);
	}
	foreach (cargo_object in this.cargos_town_pickup) {
		for (local i = cargo_object.months.len(); i <= elteltMonths; i++)
			cargo_object.months.append(0);
	}
	foreach (cargo_object in this.cargos_industry_delivery) {
		for (local i = cargo_object.months.len(); i <= elteltMonths; i++)
			cargo_object.months.append(0);
	}
	foreach (cargo_object in this.cargos_industry_pickup) {
		for (local i = cargo_object.months.len(); i <= elteltMonths; i++)
			cargo_object.months.append(0);
	}
	foreach (town_object in this.town_delivery) {
		town_object.UpdateAllMonths(elteltMonths);
	}
	foreach (town_object in this.town_pickup) {
		town_object.UpdateAllMonths(elteltMonths);
	}
	foreach (industry_object in this.industry_delivery) {
		industry_object.UpdateAllMonths(elteltMonths);
	}
	foreach (industry_object in this.industry_pickup) {
		industry_object.UpdateAllMonths(elteltMonths);
	}
}

//
//	\param cargo_id		Melyik rakomany tipusrol van szo
//	\param transported	Mennyit szallitottunk el ebbeol
//	\param month			Melyik honapban szallitottuk el
//	\param town_id		Melyik varosra vonatkozik, ez kb nem lehet null

function FGCompanyCargos::AddDeliveryTownCargo(cargo_id, transported, month, town_id) {
	if (cargo_id == null || !GSCargo.IsValidCargo(cargo_id)) {
		if (this.logLevel >= 1)
			GSLog.Error("[FGCompanyCargos::AddDeliveryTownCargo] " + GSCompany.GetName(this.company_id) + " unsuccesfull added monitoring cargo while invalid cargoID: " + cargo_id);
		return;
	}
	
	if (transported == null || transported == 0) {
		if (this.logLevel >= 1)
			GSLog.Error("[FGCompanyCargos::AddDeliveryTownCargo] " + GSCompany.GetName(this.company_id) + " unsuccesfull added monitoring cargo: " + GSCargo.GetCargoLabel(cargo_id) + " while invalid transported: " + transported);
		return;
	}
	
	/*
	 *	Eloszor hozzaadjuk az osszes deliveryhez
	 */
	
	local cargo_object = this.CargoObject(cargo_id, this.cargos_town_delivery);

	// ha nincs meg cargo hozzaadva, akkor hozzaadjuk
	if (cargo_object == null) {
		cargo_object = {cargo_id = cargo_id, months = []};
		this.cargos_town_delivery.append(cargo_object);
		if (this.logLevel >= 3)
			GSLog.Info("[FGCompanyCargos::AddDeliveryTownCargo] " + GSCompany.GetName(this.company_id) + " succesfull added monitoring new cargo: " + GSCargo.GetCargoLabel(cargo_id) + "  to All Town");
	}
	
	// leellenorizzuk, hogy a cargonak minden honapban van-e bejegyzett valamije
	for (local i = cargo_object.months.len(); i <= month; i++)
		cargo_object.months.append(0);
	
	cargo_object.months[month] = cargo_object.months[month] + transported;
	
	/*
	 *	Most meg hozzaadjuk kulon a townhoz
	 */
	
	if (town_id == null || !GSTown.IsValidTown(town_id)) {
		if (this.logLevel >= 1)
			GSLog.Error("[FGCompanyCargos::AddDeliveryTownCargo] " + GSCompany.GetName(this.company_id) + " unsuccesfull added monitoring cargo: " + GSCargo.GetCargoLabel(cargo_id) + "  to Town while invalid townID: " + town_id);
		return;
	}
	
	// itt mar csak konkretan az adott varos szamlalojahoz adjuk hozza
	local town_object = this.TownObject(town_id, this.town_delivery);
	if (town_object == null) {
		// nincs meg ilyen town hozzaadjuk
		if (this.logLevel >= 3)
			GSLog.Info("[FGCompanyCargos::AddDeliveryTownCargo] " + GSCompany.GetName(this.company_id) + " succesfull added to monitoring town: " + GSTown.GetName(town_id));
		town_object = FGTownCargos(this.company_id, town_id, this.logLevel);
		this.town_delivery.append(town_object);
	}
	
	cargo_object = town_object.CargoObject(cargo_id, town_object.cargos_delivery);
	
	// ha nincs meg cargo hozzaadva, akkor hozzaadjuk
	if (cargo_object == null) {
		cargo_object = {cargo_id = cargo_id, months = []};
		town_object.cargos_delivery.append(cargo_object);
		if (this.logLevel >= 3)
			GSLog.Info("[FGCompanyCargos::AddDeliveryTownCargo] " + GSCompany.GetName(this.company_id) + " succesfull added to monitoring new cargo: " + GSCargo.GetCargoLabel(cargo_id) + " to town: " + GSTown.GetName(town_id));
	}
	
	// leellenorizzuk, hogy a cargonak minden honapban van-e bejegyzett valamije
	for (local i = cargo_object.months.len(); i <= month; i++)
		cargo_object.months.append(0);
	
	cargo_object.months[month] = cargo_object.months[month] + transported;
}

function FGCompanyCargos::AddPickupTownCargo(cargo_id, transported, month, town_id) {
	if (cargo_id == null || !GSCargo.IsValidCargo(cargo_id)) {
		if (this.logLevel >= 1)
			GSLog.Error("[FGCompanyCargos::AddPickupTownCargo] " + GSCompany.GetName(this.company_id) + " unsuccesfull added monitoring cargo while invalid cargoID: " + cargo_id);
		return;
	}
	
	if (transported == null || transported == 0) {
		if (this.logLevel >= 1)
			GSLog.Error("[FGCompanyCargos::AddPickupTownCargo] " + GSCompany.GetName(this.company_id) + " unsuccesfull added monitoring cargo: " + GSCargo.GetCargoLabel(cargo_id) + " while invalid transported: " + transported);
		return;
	}
	
	/*
	 *	Eloszor hozzaadjuk az osszes pickupjahoz
	 */
	
	local cargo_object = this.CargoObject(cargo_id, this.cargos_town_pickup);

	// ha nincs meg cargo hozzaadva, akkor hozzaadjuk
	if (cargo_object == null) {
		cargo_object = {cargo_id = cargo_id, months = []};
		this.cargos_town_pickup.append(cargo_object);
		if (this.logLevel >= 3)
			GSLog.Info("[FGCompanyCargos::AddPickupTownCargo] " + GSCompany.GetName(this.company_id) + " succesfull added monitoring new cargo: " + GSCargo.GetCargoLabel(cargo_id) + "  to All Town");
	}
	
	// leellenorizzuk, hogy a cargonak minden honapban van-e bejegyzett valamije
	for (local i = cargo_object.months.len(); i <= month; i++)
		cargo_object.months.append(0);
	
	cargo_object.months[month] = cargo_object.months[month] + transported;
	
	/*
	 *	Most meg hozzaadjuk kulon a townhoz
	 */
	
	if (town_id == null || !GSTown.IsValidTown(town_id)) {
		if (this.logLevel >= 1)
			GSLog.Error("[FGCompanyCargos::AddPickupTownCargo] " + GSCompany.GetName(this.company_id) + " unsuccesfull added monitoring cargo: " + GSCargo.GetCargoLabel(cargo_id) + "  to Town while invalid townID: " + town_id);
		return;
	}
	
	// itt mar csak konkretan az adott varos szamlalojahoz adjuk hozza
	local town_object = this.TownObject(town_id, this.town_pickup);
	if (town_object == null) {
		// nincs meg ilyen town hozzaadjuk
		if (this.logLevel >= 3)
			GSLog.Info("[FGCompanyCargos::AddPickupTownCargo] " + GSCompany.GetName(this.company_id) + " succesfull added to monitoring town: " + GSTown.GetName(town_id));
		town_object = FGTownCargos(this.company_id, town_id, this.logLevel);
		this.town_pickup.append(town_object);
	}
	
	cargo_object = town_object.CargoObject(cargo_id, town_object.cargos_pickup);
	
	// ha nincs meg cargo hozzaadva, akkor hozzaadjuk
	if (cargo_object == null) {
		cargo_object = {cargo_id = cargo_id, months = []};
		town_object.cargos_pickup.append(cargo_object);
		if (this.logLevel >= 3)
			GSLog.Info("[FGCompanyCargos::AddPickupTownCargo] " + GSCompany.GetName(this.company_id) + " succesfull added to monitoring new cargo: " + GSCargo.GetCargoLabel(cargo_id) + " to town: " + GSTown.GetName(town_id));
	}
	
	// leellenorizzuk, hogy a cargonak minden honapban van-e bejegyzett valamije
	for (local i = cargo_object.months.len(); i <= month; i++)
		cargo_object.months.append(0);
	
	cargo_object.months[month] = cargo_object.months[month] + transported;
}

function FGCompanyCargos::AddDeliveryIndustryCargo(cargo_id, transported, month, industry_id) {
	if (cargo_id == null || !GSCargo.IsValidCargo(cargo_id)) {
		if (this.logLevel >= 1)
			GSLog.Error("[FGCompanyCargos::AddDeliveryIndustryCargo] " + GSCompany.GetName(this.company_id) + " unsuccesfull added monitoring cargo while invalid cargoID: " + cargo_id);
		return;
	}
	
	if (transported == null || transported == 0) {
		if (this.logLevel >= 1)
			GSLog.Error("[FGCompanyCargos::AddDeliveryIndustryCargo] " + GSCompany.GetName(this.company_id) + " unsuccesfull added monitoring cargo: " + GSCargo.GetCargoLabel(cargo_id) + " while invalid transported: " + transported);
		return;
	}
	
	/*
	 *	Eloszor hozzaadjuk az osszes deliveryhez
	 */
	
	local cargo_object = this.CargoObject(cargo_id, this.cargos_industry_delivery);

	// ha nincs meg cargo hozzaadva, akkor hozzaadjuk
	if (cargo_object == null) {
		cargo_object = {cargo_id = cargo_id, months = []};
		this.cargos_industry_delivery.append(cargo_object);
		if (this.logLevel >= 3)
			GSLog.Info("[FGCompanyCargos::AddDeliveryIndustryCargo] " + GSCompany.GetName(this.company_id) + " succesfull added monitoring new cargo: " + GSCargo.GetCargoLabel(cargo_id));
	}
	
	// leellenorizzuk, hogy a cargonak minden honapban van-e bejegyzett valamije
	for (local i = cargo_object.months.len(); i <= month; i++)
		cargo_object.months.append(0);
	
	cargo_object.months[month] = cargo_object.months[month] + transported;
	
	/*
	 *	Most meg hozzaadjuk kulon a townhoz
	 */
	
	if (industry_id == null || !GSIndustry.IsValidIndustry(industry_id)) {
		if (this.logLevel >= 1)
			GSLog.Error("[FGCompanyCargos::AddDeliveryIndustryCargo] " + GSCompany.GetName(this.company_id) + " unsuccesfull added monitoring cargo: " + GSCargo.GetCargoLabel(cargo_id) + "  to Industry while invalid industryID: " + industry_id);
		return;
	}
	
	// itt mar csak konkretan az adott varos szamlalojahoz adjuk hozza
	local industry_object = this.IndustryObject(industry_id, this.industry_delivery);
	if (industry_object == null) {
		// nincs meg ilyen town hozzaadjuk
		if (this.logLevel >= 3)
			GSLog.Info("[FGCompanyCargos::AddDeliveryIndustryCargo] " + GSCompany.GetName(this.company_id) + " succesfull added to monitoring industry: " + GSIndustry.GetName(industry_id));
		industry_object = FGIndustryCargos(this.company_id, industry_id, this.logLevel);
		this.industry_delivery.append(industry_object);
	}
	
	local cargo_object = industry_object.CargoObject(cargo_id, industry_object.cargos_delivery);
	
	// ha nincs meg cargo hozzaadva, akkor hozzaadjuk
	if (cargo_object == null) {
		cargo_object = {cargo_id = cargo_id, months = []};
		industry_object.cargos_delivery.append(cargo_object);
		if (this.logLevel >= 3)
			GSLog.Info("[FGCompanyCargos::AddDeliveryIndustryCargo] " + GSCompany.GetName(this.company_id) + " succesfull added to monitoring new cargo: " + GSCargo.GetCargoLabel(cargo_id) + " to industry: " + GSIndustry.GetName(industry_id));
	}
	
	// leellenorizzuk, hogy a cargonak minden honapban van-e bejegyzett valamije
	for (local i = cargo_object.months.len(); i <= month; i++)
		cargo_object.months.append(0);
	
	cargo_object.months[month] = cargo_object.months[month] + transported;
}

function FGCompanyCargos::AddPickupIndustryCargo(cargo_id, transported, month, industry_id) {
	if (cargo_id == null || !GSCargo.IsValidCargo(cargo_id)) {
		if (this.logLevel >= 1)
			GSLog.Error("[FGCompanyCargos::AddPickupIndustryCargo] " + GSCompany.GetName(this.company_id) + " unsuccesfull added monitoring cargo while invalid cargoID: " + cargo_id);
		return;
	}
	
	if (transported == null || transported == 0) {
		if (this.logLevel >= 1)
			GSLog.Error("[FGCompanyCargos::AddPickupIndustryCargo] " + GSCompany.GetName(this.company_id) + " unsuccesfull added monitoring cargo: " + GSCargo.GetCargoLabel(cargo_id) + " while invalid transported: " + transported);
		return;
	}
	
	/*
	 *	Eloszor hozzaadjuk az osszes pickupjahoz
	 */
	
	local cargo_object = this.CargoObject(cargo_id, this.cargos_industry_pickup);

	// ha nincs meg cargo hozzaadva, akkor hozzaadjuk
	if (cargo_object == null) {
		cargo_object = {cargo_id = cargo_id, months = []};
		this.cargos_industry_pickup.append(cargo_object);
		if (this.logLevel >= 3)
			GSLog.Info("[FGCompanyCargos::AddPickupIndustryCargo] " + GSCompany.GetName(this.company_id) + " succesfull added monitoring new cargo: " + GSCargo.GetCargoLabel(cargo_id));
	}
	
	// leellenorizzuk, hogy a cargonak minden honapban van-e bejegyzett valamije
	for (local i = cargo_object.months.len(); i <= month; i++)
		cargo_object.months.append(0);
	
	cargo_object.months[month] = cargo_object.months[month] + transported;
	
	/*
	 *	Most meg hozzaadjuk kulon a townhoz
	 */
	
	if (industry_id == null || !GSIndustry.IsValidIndustry(industry_id)) {
		if (this.logLevel >= 1)
			GSLog.Error("[FGCompanyCargos::AddPickupIndustryCargo] " + GSCompany.GetName(this.company_id) + " unsuccesfull added monitoring cargo: " + GSCargo.GetCargoLabel(cargo_id) + "  to Industry while invalid industryID: " + industry_id);
		return;
	}
	
	local industry_object = this.IndustryObject(industry_id, this.industry_pickup);
	if (industry_object == null) {
		// nincs meg ilyen town hozzaadjuk
		if (this.logLevel >= 3)
			GSLog.Info("[FGCompanyCargos::AddPickupIndustryCargo] " + GSCompany.GetName(this.company_id) + " succesfull added to monitoring industry: " + GSIndustry.GetName(industry_id));
		industry_object = FGIndustryCargos(this.company_id, industry_id, this.logLevel);
		this.industry_pickup.append(industry_object);
	}
	
	local cargo_object = industry_object.CargoObject(cargo_id, industry_object.cargos_pickup);
	
	// ha nincs meg cargo hozzaadva, akkor hozzaadjuk
	if (cargo_object == null) {
		cargo_object = {cargo_id = cargo_id, months = []};
		industry_object.cargos_pickup.append(cargo_object);
		if (this.logLevel >= 3)
			GSLog.Info("[FGCompanyCargos::AddPickupIndustryCargo] " + GSCompany.GetName(this.company_id) + " succesfull added to monitoring new cargo: " + GSCargo.GetCargoLabel(cargo_id) + " to industry: " + GSIndustry.GetName(industry_id));
	}
	
	// leellenorizzuk, hogy a cargonak minden honapban van-e bejegyzett valamije
	for (local i = cargo_object.months.len(); i <= month; i++)
		cargo_object.months.append(0);
	
	cargo_object.months[month] = cargo_object.months[month] + transported;
	
	/*
	// TODO: lehet ezt be kellene tenni, de akkor a deliverybe is felette
	// ha ez aktiv, akkor a varoshoz is hozzaadja a felvett cuccot. ez viszont duplikalashoz vezethet, ha a town_pickupot hasznalom jelenleg
	// szoval ez most hulyeseg, akkor lenne ertelme, ha kijavitanak a hibat, de akkor sem lenne jo, hogy ez is kozrejatszik, mert ha kivancsi az osszes pickupra, akkor iratkozzon fel ra. na ez lesz :D
	// industry eseteben visszalinkelem, es az indusstry tulajdons townhoz is hozzaadom az elszallitottat, igy a vegen eleg csak a town-t lekerdezni
	local town_id = GSTile.GetTownAuthority(GSIndustry.GetLocation(industry_id))
	if (town_id != null && GSTown.IsValidTown(town_id)) {
		this.AddPickupTownCargo(cargo_id, transported, month, town_id);
	}
	 */
}

function FGCompanyCargos::CargoObject(cargo_id, cargos) {
	foreach (cargo_object in cargos) {
		if (cargo_object.cargo_id == cargo_id)
			return cargo_object;
	}
	
	return null;
}

// towns lehet this.town_delivery or this.town_pickup
function FGCompanyCargos::TownObject(town_id, towns) {
	foreach (town_object in towns) {
		if (town_object.town_id == town_id)
			return town_object;
	}
	
	return null;
}

// industries lehet this.industry_delivery or this.industry_pickup
function FGCompanyCargos::IndustryObject(industry_id, industries) {
	foreach (industry_object in industries) {
		if (industry_object.industry_id == industry_id)
			return industry_object;
	}
	
	return null;
}

// subscriber-t var, mert abban minden info benne van
function FGCompanyCargos::Remove(subscriber, subscribers) {
	// a subscribers mar nem tartalmazza ezt a subscribert, szoval nem kell goal_id-ra szurnunk
	local need_monitoring_town_cargos_delivery = [];
	local need_monitoring_town_cargos_pickup = [];
	local need_monitoring_industry_cargos_delivery = [];
	local need_monitoring_industry_cargos_pickup = [];
	
	// eloszor bepakolom azokat, amikre kesobb is szukseg lesz
	foreach (asubscriber in subscribers) {
		if (asubscriber.company_id != subscriber.company_id || asubscriber.cargo_id != subscriber.cargo_id)
			continue;
		
		switch (asubscriber.monitor_type) {
			case FGCargoMonitor.MONITOR_ALL:
				// elvileg ebben az eset szukseg van tovabb a monitorozasra, ezert kilepunk
				if (this.logLevel >= 3)
					GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_ALL) still have monitoring cargo_id: " + subscriber.cargo_id);
				return true;
				break;
			case FGCargoMonitor.MONITOR_ALL_DELIVERY:
				if (subscriber.monitor_type == FGCargoMonitor.MONITOR_ALL ||
					subscriber.monitor_type == FGCargoMonitor.MONITOR_ALL_DELIVERY ||
					subscriber.monitor_type == FGCargoMonitor.MONITOR_TOWN_DELIVERY ||
					subscriber.monitor_type == FGCargoMonitor.MONITOR_INDUSTRY_DELIVERY) {
					// tehat ha valamilyen delivery volt, akkor kilepunk, mert szuksegunk van meg az osszes deliveryre
					if (this.logLevel >= 3)
						GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_ALL_DELIVERY) still have monitoring cargo_id: " + subscriber.cargo_id);
					return true;
				}
				break;
			case FGCargoMonitor.MONITOR_ALL_PICKUP:
				if (subscriber.monitor_type == FGCargoMonitor.MONITOR_ALL ||
					subscriber.monitor_type == FGCargoMonitor.MONITOR_ALL_PICKUP ||
					subscriber.monitor_type == FGCargoMonitor.MONITOR_TOWN_PICKUP ||
					subscriber.monitor_type == FGCargoMonitor.MONITOR_INDUSTRY_PICKUP) {
					// tehat ha valamilyen pickup volt, akkor kilepunk, mert szuksegunk van meg az osszes pickupra
					if (this.logLevel >= 3)
						GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_ALL_PICKUP) still have monitoring cargo_id: " + subscriber.cargo_id);
					return true;
				}
				break;
			case FGCargoMonitor.MONITOR_TOWN_DELIVERY:
				need_monitoring_town_cargos_delivery.append(asubscriber.type_id);
				break;
			case FGCargoMonitor.MONITOR_TOWN_PICKUP:
				need_monitoring_town_cargos_pickup.append(asubscriber.type_id);
				// TODO: ezt is ki kell venni, ha megoldjak a townpickupot industry nelkul
				// itt ugye meg szukseg van az adott varos osszes industry-ara, ami ezt a cargot gyartja
				local indlist = GSIndustryList_CargoProducing(asubscriber.cargo_id);
				foreach (industry_id, _ in indlist) {
					local town_author = GSTile.GetTownAuthority(GSIndustry.GetLocation(industry_id));
					if (town_author == asubscriber.type_id)
						need_monitoring_industry_cargos_pickup.append(industry_id);
				}
				break;
			case FGCargoMonitor.MONITOR_INDUSTRY_DELIVERY:
				need_monitoring_industry_cargos_delivery.append(asubscriber.type_id);
				break;
			case FGCargoMonitor.MONITOR_INDUSTRY_PICKUP:
				need_monitoring_industry_cargos_pickup.append(asubscriber.type_id);
				break;
		}
	}
	
	local need_remove_all_delivery = false;
	local need_remove_all_pickup = false;
	
	local compname = GSCompany.GetName(this.company_id);
	if (compname == null || compname == "")
		compname = "(Bankrupted or Merged Company)";
	
	switch (subscriber.monitor_type) {
		case FGCargoMonitor.MONITOR_ALL:
			need_remove_all_delivery = true;
			need_remove_all_pickup = true;
			// az osszes delivery es pickupot el kell tavolitani, kiveve azokat, amikre sziksegunk van meg
			// elivleg erre a cergora nincs all monitoring, mert ha lenne, mar kileptunk volna
			// tehat itt mindarrol le kell iratkozni, ami nincs benne a tobbiben
			break;
		case FGCargoMonitor.MONITOR_ALL_DELIVERY:
			need_remove_all_delivery = true;
			break;
		case FGCargoMonitor.MONITOR_ALL_PICKUP:
			need_remove_all_pickup = true;
			break;
		case FGCargoMonitor.MONITOR_TOWN_DELIVERY:
			// itt az adott varos deliverytol iratkozunk le, ha nincs benne a kozosben
			local van = false;
			foreach (a_town_id in need_monitoring_town_cargos_delivery) {
				if (subscriber.type_id == a_town_id) {
					if (this.logLevel >= 3)
						GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_TOWN_DELIVERY) " + compname + " still have cargo (cargo_id: " + subscriber.cargo_id + ") town delivery monitoring");
					van = true;
					break;
				}
			}
			
			if (!van) {
				// tul sokszor jelenik meg ez az uzenet, azert tavolitom el
//				if (this.logLevel >= 3)
//					GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_TOWN_DELIVERY) " + compname + " removing cargo (cargo_id: " + subscriber.cargo_id + ") town delivery monitoring");
				
				local town_object = this.TownObject(subscriber.type_id, this.town_delivery);
				// feleslegesen ne dobjon hibat
				if (town_object != null) {
					this.RemoveTownCargo(subscriber.cargo_id, subscriber.type_id, this.town_delivery);
					if (subscriber.type_id != null && GSTown.IsValidTown(subscriber.type_id)) {
						if (this.logLevel >= 3)
							GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_TOWN_DELIVERY) " + compname + " removing cargo from town: " + GSTown.GetName(subscriber.type_id));
						GSCargoMonitor.GetTownDeliveryAmount(this.company_id, subscriber.cargo_id, subscriber.type_id, false);
					}
				}
			}
			break;
		case FGCargoMonitor.MONITOR_TOWN_PICKUP:
			// itt az adott varos pickuprol iratkozunk le, ha nincs benne a kozosben
			local van = false;
			foreach (a_town_id in need_monitoring_town_cargos_pickup) {
				if (subscriber.type_id == a_town_id) {
					if (this.logLevel >= 3)
						GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_TOWN_PICKUP) " + compname + " still have cargo (cargo_id: " + subscriber.cargo_id + ") town pickup monitoring");
					van = true;
					break;
				}
			}
			
			if (!van) {
				// tul sokszor jelenik meg ez az uzenet, azert tavolitom el
//				if (this.logLevel >= 3)
//					GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_TOWN_PICKUP) " + compname + " removing cargo (cargo_id: " + subscriber.cargo_id + ") town pickup monitoring");
				
				local town_object = this.TownObject(subscriber.type_id, this.town_pickup);
				// feleslegesen ne dobjon hibat
				if (town_object != null) {
					this.RemoveTownCargo(subscriber.cargo_id, subscriber.type_id, this.town_pickup);
					if (subscriber.type_id != null && GSTown.IsValidTown(subscriber.type_id)) {
						if (this.logLevel >= 3)
							GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_TOWN_PICKUP) " + compname + " removing cargo from town: " + GSTown.GetName(subscriber.type_id));
						GSCargoMonitor.GetTownPickupAmount(this.company_id, subscriber.cargo_id, subscriber.type_id, false);
					}
				}
			}
			
			// TODO: ezt is ki kell venni, ha megoldjak a townpickupot industry nelkul (mint fentebb)
			local indlist = GSIndustryList_CargoProducing(subscriber.cargo_id);
			// csak az adott varosban levo gyarakrol iratkozhatunk le
			local townindlist = [];
			foreach (industry_id, _ in indlist) {
				local town_author = GSTile.GetTownAuthority(GSIndustry.GetLocation(industry_id));
				if (town_author == subscriber.type_id)
					townindlist.append(industry_id);
			}
			
			foreach (industry_id in townindlist) {
				local van = false;
				foreach (a_industry_id in need_monitoring_industry_cargos_pickup) {
					if (industry_id == a_industry_id) {
						if (this.logLevel >= 3)
							GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_TOWN_PICKUP) " + compname + " still have cargo (cargo_id: " + subscriber.cargo_id + ") industry pickup monitoring in industry: " + GSIndustry.GetName(industry_id));
						van = true;
						break;
					}
				}
				
				if (!van) {
					// tul sokszor jelenik meg ez az uzenet, azert tavolitom el
//					if (this.logLevel >= 3)
//						GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_TOWN_PICKUP) " + compname + " removing cargo (cargo_id: " + subscriber.cargo_id + ") from industry pickup monitoring in town: " + GSTown.GetName(subscriber.type_id));
					
					local industry_object = this.IndustryObject(industry_id, this.industry_pickup);
					// feleslegesen ne dobjon hibat
					if (industry_object != null) {
						this.RemoveIndustryCargo(subscriber.cargo_id, industry_id, this.industry_pickup);
						// eltavolitjuk az industryt is is
						if (GSIndustry.IsValidIndustry(industry_id)) {
							if (this.logLevel >= 3)
								GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_TOWN_PICKUP) " + compname + " removing cargo from industry: " + GSIndustry.GetName(industry_id));
							GSCargoMonitor.GetIndustryPickupAmount(this.company_id, subscriber.cargo_id, industry_id, false);
						}
					}
				}
			}
			break;
		case FGCargoMonitor.MONITOR_INDUSTRY_DELIVERY:
			// itt mar konnyebb, mert csak akkor nem kell leiratkozni, ha szukseg van az adott industry delivery-re mashol is
			local van = false;
			foreach (a_industry_id in need_monitoring_industry_cargos_pickup) {
				if (subscriber.type_id == a_industry_id) {
					if (this.logLevel >= 3)
						GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_INDUSTRY_DELIVERY) " + compname + " still have cargo (cargo_id: " + subscriber.cargo_id + ") delivery monitoring in industry: " + GSIndustry.GetName(subscriber.type_id));
					van = true;
					break;
				}
			}
			
			if (!van) {
				// tul sokszor jelenik meg ez az uzenet, azert tavolitom el
//				if (this.logLevel >= 3)
//					GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_INDUSTRY_DELIVERY) " + compname + " removing cargo (cargo_id: " + subscriber.cargo_id + ") from industry delivery monitoring");
				
				local industry_object = this.IndustryObject(subscriber.type_id, this.industry_delivery);
				// feleslegesen ne dobjon hibat
				if (industry_object != null) {
					this.RemoveIndustryCargo(subscriber.cargo_id, subscriber.type_id, this.industry_delivery);
					// eltavolitjuk az industryt is is
					if (subscriber.type_id != null && GSIndustry.IsValidIndustry(subscriber.type_id)) {
						if (this.logLevel >= 3)
							GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_INDUSTRY_DELIVERY) " + compname + " removing cargo from industry: " + GSIndustry.GetName(subscriber.type_id));
						GSCargoMonitor.GetIndustryDeliveryAmount(this.company_id, subscriber.cargo_id, subscriber.type_id, false);
					}
				}
			}
			break;
		case FGCargoMonitor.MONITOR_INDUSTRY_PICKUP:
			// itt mar konnyebb, mert csak akkor nem kell leiratkozni, ha szukseg van az adott industry pickup-ra mashol is
			local van = false;
			foreach (a_industry_id in need_monitoring_industry_cargos_pickup) {
				if (subscriber.type_id == a_industry_id) {
					if (this.logLevel >= 3)
						GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_INDUSTRY_PICKUP) " + compname + " still have cargo (cargo_id: " + subscriber.cargo_id + ") industry pickup monitoring in industry: " + GSIndustry.GetName(subscriber.type_id));
					van = true;
					break;
				}
			}
			
			if (!van) {
				// tul sokszor jelenik meg ez az uzenet, azert tavolitom el
//				if (this.logLevel >= 3)
//					GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_INDUSTRY_PICKUP) " + compname + " removing cargo (cargo_id: " + subscriber.cargo_id + ") from industry pickup monitoring in town: " + GSTown.GetName(subscriber.type_id));
				
				local industry_object = this.IndustryObject(subscriber.type_id, this.industry_pickup);
				// feleslegesen ne dobjon hibat
				if (industry_object != null) {
					this.RemoveIndustryCargo(subscriber.cargo_id, subscriber.type_id, this.industry_pickup);
					// eltavolitjuk az industryt is is
					if (subscriber.type_id != null && GSIndustry.IsValidIndustry(subscriber.type_id)) {
						if (this.logLevel >= 3)
							GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_INDUSTRY_PICKUP) " + compname + " removing cargo from industry: " + GSIndustry.GetName(subscriber.type_id));
						GSCargoMonitor.GetIndustryPickupAmount(this.company_id, subscriber.cargo_id, subscriber.type_id, false);
					
					}
				}
			}
			break;
	}
	
	local townlist = GSTownList();
	if (need_remove_all_delivery) {
		// ha van need_mon del-ben valami, akkor a kozosbol nem kell eltavolitani amugy igen
		if (need_monitoring_town_cargos_delivery.len() == 0) {
			if (this.logLevel >= 3)
				GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_ALL_DELIVERY) " + compname + " removing cargo (cargo_id: " + subscriber.cargo_id + ") from all delivery monitoring");
			this.RemoveCargo(subscriber.cargo_id, this.cargos_town_delivery);
			this.RemoveCargo(subscriber.cargo_id, this.cargos_industry_delivery);
		}
		
		// most meg eltavolitom az osszes town cargot, ami nem kell
		foreach (town_id, _ in townlist) {
			local van = false;
			foreach (a_town_id in need_monitoring_town_cargos_delivery) {
				if (town_id == a_town_id) {
					if (this.logLevel >= 3)
						GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_ALL_DELIVERY) " + compname + " still have cargo (cargo_id: " + subscriber.cargo_id + ") delivery monitoring in town: " + GSTown.GetName(town_id));
					van = true;
					break;
				}
			}
			
			if (!van) {
				// tul sokszor jelenik meg ez az uzenet, azert tavolitom el
//				if (this.logLevel >= 3)
//					GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_ALL_DELIVERY) " + compname + " removing cargo (cargo_id: " + subscriber.cargo_id + ") from town delivery monitoring");
				
				local town_object = this.TownObject(town_id, this.town_delivery);
				// feleslegesen ne dobjon hibat
				if (town_object != null) {
					this.RemoveTownCargo(subscriber.cargo_id, town_id, this.town_delivery);
					// eltavolitjuk a varost is
					if (GSTown.IsValidTown(town_id)) {
						if (this.logLevel >= 3)
							GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_ALL_DELIVERY) " + compname + " removing cargo from town: " + GSTown.GetName(town_id));
						GSCargoMonitor.GetTownDeliveryAmount(this.company_id, subscriber.cargo_id, town_id, false);
					}
				}
			}
		}
	}
	
	if (need_remove_all_pickup) {
		// ha van need_mon del-ben valami, akkor a kozosbol nem kell eltavolitani amugy igen
		if (need_monitoring_town_cargos_pickup.len() == 0) {
			if (this.logLevel >= 3)
				GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_ALL_PICKUP) " + compname + " removing cargo (cargo_id: " + subscriber.cargo_id + ") from all pickup monitoring");
			this.RemoveCargo(subscriber.cargo_id, this.cargos_town_pickup);
			this.RemoveCargo(subscriber.cargo_id, this.cargos_industry_pickup);
		}
		
		// most meg eltavolitom az osszes town cargot, ami nem kell
		foreach (town_id, _ in townlist) {
			local van = false;
			foreach (a_town_id in need_monitoring_town_cargos_pickup) {
				if (town_id == a_town_id) {
					if (this.logLevel >= 3)
						GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_ALL_PICKUP) " + compname + " still have cargo (cargo_id: " + subscriber.cargo_id + ") pickup monitoring in town: " + GSTown.GetName(town_id));
					van = true;
					break;
				}
			}
			
			if (!van) {
				// tul sokszor jelenik meg ez az uzenet, azert tavolitom el
//				if (this.logLevel >= 3)
//					GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_ALL_PICKUP) " + compname + " removing cargo (cargo_id: " + subscriber.cargo_id + ") from town pickup monitoring");
				
				local town_object = this.TownObject(town_id, this.town_pickup);
				// feleslegesen ne dobjon hibat
				if (town_object != null) {
					this.RemoveTownCargo(subscriber.cargo_id, town_id, this.town_pickup);
					// eltavolitjuk a varost is
					if (GSTown.IsValidTown(town_id)) {
						if (this.logLevel >= 3)
							GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_ALL_PICKUP) " + compname + " removing cargo from town: " + GSTown.GetName(town_id));
						GSCargoMonitor.GetTownPickupAmount(this.company_id, subscriber.cargo_id, town_id, false);
					}
				}
			}
		}
		
		// TODO: ezt is ki kell venni, ha megoldjak a townpickupot industry nelkul
		// itt el kell tavolitani az osszes industry pickupot, ami a listaban nincs benne
		local indlist = GSIndustryList_CargoProducing(subscriber.cargo_id);
		foreach (industry_id, _ in indlist) {
			local van = false;
			foreach (a_industry_id in need_monitoring_industry_cargos_pickup) {
				if (industry_id == a_industry_id) {
					if (this.logLevel >= 3)
						GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_ALL_PICKUP) " + compname + " still have cargo (cargo_id: " + subscriber.cargo_id + ") pickup monitoring in industry: " + GSIndustry.GetName(industry_id));
					van = true;
					break;
				}
			}
			
			if (!van) {
				// tul sokszor jelenik meg ez az uzenet, azert tavolitom el
//				if (this.logLevel >= 3)
//					GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_ALL_PICKUP) " + compname + " removing cargo (cargo_id: " + subscriber.cargo_id + ") from industry pickup monitoring");
				
				local industry_object = this.IndustryObject(industry_id, this.industry_pickup);
				// feleslegesen ne dobjon hibat
				if (industry_object != null) {
					this.RemoveIndustryCargo(subscriber.cargo_id, industry_id, this.industry_pickup);
					// eltavolitjuk az industryt is is
					if (GSIndustry.IsValidIndustry(industry_id)) {
						if (this.logLevel >= 3)
							GSLog.Info("[FGCompanyCargos::Remove] (MONITOR_ALL_PICKUP) " + compname + " removing cargo from industry: " + GSIndustry.GetName(industry_id));
						GSCargoMonitor.GetIndustryPickupAmount(this.company_id, subscriber.cargo_id, industry_id, false);
					}
				}
			}
		}
	}

	return true;
}

function FGCompanyCargos::RemoveCargo(cargo_id, cargos) {
	for (local i = 0; i < cargos.len(); i++) {
		if (cargos[i].cargo_id == cargo_id) {
			cargos.remove(i);
			if (this.logLevel >= 3)
				GSLog.Info("[FGCompanyCargos::RemoveCargo] " + GSCompany.GetName(this.company_id) + " succesfull unsubscribed from monitoring cargo: " + GSCargo.GetCargoLabel(cargo_id));
			break;
		}
	}
}

function FGCompanyCargos::RemoveTownCargo(cargo_id, town_id, towns) {
	local town_object = this.TownObject(town_id, towns);
	if (town_object == null) {
		if (this.logLevel >= 1)
			GSLog.Error("[FGCompanyCargos::RemoveTownCargo] " + GSCompany.GetName(this.company_id) + " unsuccesfull unsubscribed from monitoring cargo: " + GSCargo.GetCargoLabel(cargo_id) + " while INVALID town_object from town_id: " + town_id);
		return;
	}
	
	local cargos = null;
	if (towns == this.town_delivery)
		cargos = town_object.cargos_delivery;
	else
		cargos = town_object.cargos_pickup;
	
	town_object.RemoveCargo(cargo_id, cargos);
	
//	if (this.logLevel >= 3)
//		GSLog.Info("[FGCompanyCargos::RemoveTownCargo] " + GSCompany.GetName(this.company_id) + " succesfull unsubscribed from monitoring cargo: " + GSCargo.GetCargoLabel(cargo_id));
}

function FGCompanyCargos::RemoveIndustryCargo(cargo_id, industry_id, industries) {
	local industry_object = this.IndustryObject(industry_id, industries);
	if (industry_object == null) {
		if (this.logLevel >= 1)
			GSLog.Error("[FGCompanyCargos::RemoveIndustryCargo] " + GSCompany.GetName(this.company_id) + " unsuccesfull unsubscribed from monitoring cargo: " + GSCargo.GetCargoLabel(cargo_id) + " while INVALID industry_object from industry_id: " + industry_id);
		return;
	}
	
	local cargos = null;
	if (industries == this.industry_delivery)
		cargos = industry_object.cargos_delivery;
	else
		cargos = industry_object.cargos_pickup;
	
	industry_object.RemoveCargo(cargo_id, cargos);
	
//	if (this.logLevel >= 3)
//		GSLog.Info("[FGCompanyCargos::RemoveIndustryCargo] " + GSCompany.GetName(this.company_id) + " succesfull unsubscribed from monitoring cargo: " + GSCargo.GetCargoLabel(cargo_id));
}

