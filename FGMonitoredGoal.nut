class FGMonitoredGoal {
	company_id = null;
	cargo_id = null;
	goal_id = null;
	refresh_interval = null;
	monitor_type = null;
	type_id = null;
	months = null; // ebben tarolom majd kulon a feladat vegrehajtasahoz beerkezett mennyiseget
	last_months = -1; // azaz az osszeset visszakuldjuk
	
	constructor (data) {
		this.LoadFromData(data);
	}
}

function FGMonitoredGoal::SaveData() {
	return {
		company_id = this.company_id,
		cargo_id = this.cargo_id,
		goal_id = this.goal_id,
		refresh_interval = this.refresh_interval,
		monitor_type = this.monitor_type,
		type_id = this.type_id,
		months = this.months,
		last_months = this.last_months,
	}
}

function FGMonitoredGoal::LoadFromData(data) {
	this.company_id = data.company_id;
	this.cargo_id = data.cargo_id;
	this.goal_id = data.goal_id;
	this.refresh_interval = data.refresh_interval;
	this.monitor_type = data.monitor_type;
	this.type_id = data.type_id;
	this.months = data.months;
	this.last_months = data.last_months;
}

function FGMonitoredGoal::Description() {
	local addt = "";

	switch (this.monitor_type) {
		case FGCargoMonitor.MONITOR_TOWN_DELIVERY:
			addt = ", town: " + GSTown.GetName(this.type_id);
			break;
		case FGCargoMonitor.MONITOR_TOWN_PICKUP:
			addt = ", town: " + GSTown.GetName(this.type_id);
			break;
		case FGCargoMonitor.MONITOR_INDUSTRY_DELIVERY:
			addt = ", industry: " + GSIndustry.GetName(this.type_id);
			break;
		case FGCargoMonitor.MONITOR_INDUSTRY_PICKUP:
			addt = ", industry: " + GSIndustry.GetName(this.type_id);
			break;
	}

	local umonths = this.last_months;
	if (this.last_months == FGCargoMonitor.LAST_MONTH_NONE)
		umonths = "LAST_MONTH_NONE";
	else if (this.last_months == FGCargoMonitor.LAST_MONTH_ALL)
		umonths = "LAST_MONTH_ALL";
	else if (this.last_months == FGCargoMonitor.LAST_MONTH_GOAL)
		umonths = "LAST_MONTH_GOAL";
	else if (this.last_months == FGCargoMonitor.LAST_MONTH_CURRENT)
		umonths = "LAST_MONTH_CURRENT";


	return "company: " + GSCompany.GetName(this.company_id) + ", cargo label: " + GSCargo.GetCargoLabel(this.cargo_id) + ", goal_id: " + this.goal_id +
	" , monitor type: " + FGCargoMonitor.MonitorTypeName(this.monitor_type) + ", refresh type: " + FGCargoMonitor.RefreshTypeName(this.refresh_interval) + ", updates for monthst: " + umonths + addt;
}

function FGMonitoredGoal::AddTransported(transported, elteltMonths) {
	for (local i = this.months.len(); i <= elteltMonths; i++)
		this.months.append(0);

	this.months[elteltMonths] = this.months[elteltMonths] + transported;
}

// fixen a goal transportedet jeleniti meg.
function FGMonitoredGoal::GetTransported(last_months, elteltMonths, need_before_month) {
	for (local i = this.months.len(); i <= elteltMonths; i++)
		this.months.append(0);


	// ide ez nem kell, hogy le lehessen kerdezni az osszes elszallitott cuccot meg akkor is, ha az elejen LAST_MONTH_NONE-ra iratkoztak fel.
//	if (last_months == FGCargoMonitor.LAST_MONTH_NONE) {
//		return 0;
//	}
	if (last_months >= this.months.len() || last_months == FGCargoMonitor.LAST_MONTH_ALL) {
		local transported = 0;
		foreach (amount in this.months) {
			transported += amount;
		}

		return transported;
	}
	else if (last_months == FGCargoMonitor.LAST_MONTH_GOAL) {
		switch (this.refresh_interval) {
			case FGCargoMonitor.REFRESH_INVALID:
			case FGCargoMonitor.REFRESH_NONE:
				return 0;
				break;
			case FGCargoMonitor.REFRESH_IMMEDIATELY:
				last_months = 0;
				break;
			case FGCargoMonitor.REFRESH_EVERY_MONTHS:
				last_months = 0;
				break;
			case FGCargoMonitor.REFRESH_EVERY_QARTER_YEARS:
				last_months = 3;
				break;
			case FGCargoMonitor.REFRESH_EVERY_HALF_YEARS:
				last_months = 6;
				break;
			case FGCargoMonitor.REFRESH_EVERY_YEARS:
				last_months = 12;
				break;
			default:
				return 0;
				break;
		}
	}
	else if (last_months == 0) {
		local month = this.months.len() - 1;
		if (need_before_month)
			month--;

		if (this.months.len() > month)
			return this.months[month];
		else
			return 0;
	}

	local transported = 0;
	local counter = 0;

	local from = this.months.len() - 1;

	if (need_before_month && this.refresh_interval != FGCargoMonitor.REFRESH_IMMEDIATELY)
		from--;

	for (local i = from; ((counter <= last_months) && (this.months.len() > i) && i >= 0); i--) {
		counter++;
		transported += this.months[i];
	}

	return transported;
}
